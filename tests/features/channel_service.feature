Feature: Channel Service

  Background:
    Given I have a Channel Service

  Scenario: Echo a message that the Channel Service receive
    When I send I message "Hello World" to the Channel Service
    Then I should receive back a message "Hello World"