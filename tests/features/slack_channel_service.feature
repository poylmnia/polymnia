Feature: Slack Channel Service

  Scenario: Verify that a Message has been sent
    Given I have a mocked request to Slack "Hello World"
    When the Slack Channel Service sent a Message "Hello World"
    Then the Message should be sent to Slack