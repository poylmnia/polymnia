<?php

namespace Polymnia\Tests\Features;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Polymnia\Application\ChannelService;
use PHPUnit\Framework\TestCase;
use Polymnia\Infrastructure\Slack\Slack;
use WireMock\Client\WireMock;
use WireMock\Matching\UrlMatchingStrategy;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends TestCase implements Context
{

    private ChannelService $channelService;

    private Slack $slack;

    private string $response;

    private WireMock $wiremock;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        parent::__construct();
        $this->wiremock = WireMock::create('wiremock');
        $this->slack = new Slack(
            'wiremock:8080',
            '/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX'
        );
    }

    /**
     * @Given I have a Channel Service
     */
    public function iHaveAChannelService()
    {
        $this->channelService = new ChannelService();
    }

    /**
     * @When I send I message :rawMessage to the Channel Service
     * @param string $rawMessage
     */
    public function iSendIMessageToTheChannelService(string $rawMessage)
    {
        $this->response = $this->channelService->receiveRawMessage($rawMessage);
    }

    /**
     * @Then I should receive back a message :response
     * @param string $response
     */
    public function iShouldReceiveBackAMessage(string $response)
    {
        $this->assertEquals(
            $response,
            $this->response
        );
    }

    /**
     * @Given I have a mocked request to Slack :message
     */
    public function iHaveAMockedRequestToSlack(string $message)
    {
        $postData = array(
            'text' => $message
        );
        $this->assertEquals(true, $this->wiremock->isAlive());
        $this->wiremock->stubFor(
            WireMock::post(WireMock::urlEqualTo('/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX'))
                ->withRequestBody(
                    WireMock::equalToJson(json_encode($postData))
                )->willReturn(WireMock::aResponse()->withStatus(200)->withBody(
                    '{}'
                )));
    }


    /**
     * @When the Slack Channel Service sent a Message :message
     * @param string $message
     */
    public function theSlackChannelServiceSentAMessage(string $message)
    {
        $this->slack->send($message);
    }

    /**
     * @Then the Message should be sent to Slack
     */
    public function theMessageShouldBeSentToSlack()
    {
        $this->wiremock->verify(
            WireMock::postRequestedFor(
                WireMock::urlEqualTo('/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX')
            )
        );
    }
}
