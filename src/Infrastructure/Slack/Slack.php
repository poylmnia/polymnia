<?php

namespace Polymnia\Infrastructure\Slack;

class Slack
{
    private string $endpoint;

    private string $baseUrl;

    public function __construct(string $baseUrl, string $endpoint)
    {
        $this->baseUrl = $baseUrl;
        $this->endpoint = $endpoint;
    }

    public function send(string $message)
    {
        $postData = array(
            'text' => $message
        );

        $jsonArray = json_encode($postData);

        $ch = curl_init($this->baseUrl . $this->endpoint);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        if ($jsonArray !== null) {
            $json = json_encode($jsonArray);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            $contentLength = strlen($json);
        } else {
            $contentLength = 0;
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Content-Length: $contentLength",
        ));

        $response = curl_exec($ch);

        $error = curl_error($ch);

        curl_close($ch);
    }
}
