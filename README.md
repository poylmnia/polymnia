# Build and Run docker

## Build
```sh
docker build --file .docker/Dockerfile -t polymnia .
```

## Check it
```sh
docker images
```

## Run it
```
docker run --rm -p 8080:80 polymnia
```

## Check it again
```
docker ps
```

## Surf it
```
http://localhost:8080
```
